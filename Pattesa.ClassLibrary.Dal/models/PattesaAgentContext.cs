﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Pattesa.ClassLibrary.AgentDal.models
{
    class PattesaAgentContext : DbContext
    {
        public PattesaAgentContext(DbContextOptions<PattesaAgentContext> options) : base(options)
        {
        }
        public DbSet<accounts> accounts { get; set; }
    }
}
