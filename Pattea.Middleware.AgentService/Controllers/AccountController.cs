﻿#region Copyright Pattesa © 2019
//
//  NAME:       AccountController.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    Getting Account Detals
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Pattea.Middleware.AgentService.models;
using Pattesa.ClassLibrary.AgentDal.Repositorys;
using Pattesa.ClassLibrary.Common.General;


namespace Pattea.Middleware.AgentService.Controllers
{
    
    [Route("api/Accounts/")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly PattesaAgentContext agentContext;
        private readonly IAgentRepository AgentRepository;
        public AccountController(PattesaAgentContext context)
        {
            agentContext = context;
            AgentRepository = new AgentRepository();
        }

        /// <summary>
        /// Get Accounts Data with Entity
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAccoutsWithEntity/")]
        public ActionResult<List<accounts>> getAccoutsWithEntity()
        {
            try
            {
                List<accounts> lst = new List<accounts>();
                lst = agentContext.accounts.ToList();
                return lst;
            }

            catch (Exception e)
            {
                Helper.SendSlackExceptionMsg($"Error from GetBilling Summary: hello", "GetBillingSummary");
                Helper.SendSlackEventMsg($"Error from GetBilling Summary: hello");
                Helper.LogException(e);
                return NotFound();
            }
        }

        /// <summary>
        /// Get Accounts Data By With Out Entity(Here using wrapper class)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("getAccoutsWithoutEntity/")]
        public ActionResult<List<accounts>> getAccoutsWithoutEntity()
        {
            try
            {
                var products = AgentRepository.GetAllCustomers();
                return new OkObjectResult(products);
            }
            catch (Exception e)
            {
                Helper.SendSlackExceptionMsg($"Error from GetBilling Summary: hello", "GetBillingSummary");
                Helper.SendSlackEventMsg($"Error from GetBilling Summary: hello");
                Helper.LogException(e);
                return NotFound();
            }
        }
    }
}