﻿#region Copyright Pattesa © 2019
//
//  NAME:       PattesaAgentContext.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    Inject All Database Models
//
#endregion

using Microsoft.EntityFrameworkCore;
namespace Pattea.Middleware.AgentService.models
{
    public class PattesaAgentContext : DbContext
    {
        public PattesaAgentContext(DbContextOptions<PattesaAgentContext> options) : base(options)
        {
        }
        public DbSet<accounts> accounts { get; set; }
    }
}
