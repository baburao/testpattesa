﻿#region Copyright Pattesa © 2019
//
//  NAME:       accounts.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    Account Model File
//
#endregion

using System.ComponentModel.DataAnnotations;

namespace Pattea.Middleware.AgentService.models
{
    public class accounts
    {
        [Key]
        public int id1 { get; set; }
        [Required]
        [Display(Name = "name")]
        public string name { get; set; }
        [Required]
        [Display(Name = "balance")]
        public int balance { get; set; }
    }
}
