﻿#region Copyright Pattesa © 2019
//
//  NAME:       Interface1.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    It Contains All Interfaces Methods
//
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using Pattesa.ClassLibrary.AgentDal.models;
using Pattesa.ClassLibrary.AgentDal.Repositorys;

namespace Pattesa.ClassLibrary.AgentDal
{
    public interface Interface1
    {
        object GetCartNames();
    }
}
