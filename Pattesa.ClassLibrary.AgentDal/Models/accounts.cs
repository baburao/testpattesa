﻿#region Copyright Pattesa © 2019
//
//  NAME:       accounts.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    Account Model File
//
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
namespace Pattesa.ClassLibrary.AgentDal.models
{
    class accounts
    {
        [Key]
        public int id { get; set; }
        [Required]
        [Display(Name = "name")]
        public string name { get; set; }
        [Required]
        [Display(Name = "balance")]
        public int balance { get; set; }
    }
}
