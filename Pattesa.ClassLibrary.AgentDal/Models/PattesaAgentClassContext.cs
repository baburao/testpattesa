﻿#region Copyright Pattesa © 2019
//
//  NAME:       PattesaAgentContext.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    Inject All Database Models
//
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Pattesa.ClassLibrary.AgentDal.models
{
    class PattesaAgentClassContext : DbContext
    {
        public PattesaAgentClassContext()
        {

        }

        public PattesaAgentClassContext(DbContextOptions<PattesaAgentClassContext> options) : base(options)
        {
        }
        public DbSet<accounts> accounts { get; set; }
    }
}
