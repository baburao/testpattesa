﻿#region Copyright Pattesa © 2019
//
//  NAME:       IAgentRepository.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    It Contains All Interfaces Methods
//
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using Pattesa.ClassLibrary.AgentDal.models;

namespace Pattesa.ClassLibrary.AgentDal.Repositorys
{
    public interface IAgentRepository
    {
        object GetCart();

        object getCountries();

        object GetAllCustomers();
    }
}
