﻿#region Copyright Pattesa © 2019
//
//  NAME:       AgentRepository.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    AgentRepository Includes stored procedures functionalitys
//
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Pattesa.ClassLibrary.AgentDal.models;
using Pattesa.ClassLibrary.AgentDal.Repositorys;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Data.Common;
using Npgsql;
using Pattesa.ClassLibrary.Common.General;




namespace Pattesa.ClassLibrary.AgentDal.Repositorys
{
    public class AgentRepository : IAgentRepository,Interface1
    {
       
        private readonly PattesaAgentClassContext agentContext;
        string connectionString = "Host=localhost;Port=5432;Username=postgres;Password=sa2012;Database=postgres;";

        public AgentRepository()
        {
            agentContext = new PattesaAgentClassContext();
        }

        public object GetCart()
        {
            return "hello";
        }

        public object getCountries()
        {
            List<accounts> lst = new List<accounts>();
            try
            {
                
                lst = agentContext.accounts.ToList();
                
            }
            catch (Exception e)
            {
                Helper.SendSlackExceptionMsg($"Error from GetBilling Summary: hello", "GetBillingSummary");
                Helper.SendSlackEventMsg($"Error from GetBilling Summary: hello");
                Helper.LogException(e);

                
            }
            return lst;
        }

        public object GetCartNames()
        {
            throw new NotImplementedException();
        }

        public object GetAllCustomers()
        {
            List<accounts> lstCustomer = new List<accounts>();
            try
            {
                

                //var conn = "Server=localhost;Port=5432;Database=myDataBase;User Id=myUsername;Password=myPassword;";

                //using (DbConnection conn = new Npgsql.NpgsqlConnection(connectionString))
                //{
                //using (var cmd = new Npgsql.NpgsqlCommand("my_func", conn))
                //    {
                //        cmd.CommandType = CommandType.StoredProcedure;
                //        cmd.Parameters.AddWithValue("p1", "some_value");
                //        using (var reader = cmd.ExecuteReader()) { ... }
                //    }
                //string mySelectQuery = "SELECT id, name,balance FROM public.accounts";
                NpgsqlConnection pgConnection = new NpgsqlConnection(connectionString);
                //NpgsqlCommand pgCommand = new NpgsqlCommand(mySelectQuery, pgConnection);
                //pgConnection.Open();
                //NpgsqlDataReader pgReader = pgCommand.ExecuteReader();
                //try
                //{
                //    while (pgReader.Read())
                //    {
                //        Console.WriteLine(pgReader.GetInt32(0).ToString() + ", " + pgReader.GetString(1));
                //    }
                //}
                //finally
                //{
                //    // always call Close when done reading.
                //    pgReader.Close();
                //    // always call Close when done reading.
                //    pgConnection.Close();
                //}
                ////}
                ///
                pgConnection.Open();
                using (var cmd = new NpgsqlCommand("select * from public.sql_with_rows11(1)", pgConnection))

                using (var reader = cmd.ExecuteReader())
                {
                    Console.WriteLine(reader);
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        accounts student = new accounts();
                        student.id = Convert.ToInt32(dt.Rows[i]["id"]);
                        student.name = dt.Rows[i]["name"].ToString();
                        student.balance = Convert.ToInt32(dt.Rows[i]["balance"]);

                        lstCustomer.Add(student);
                    }
                }

                //using (var dr = cmd.ExecuteReader())
                //{


                //    while (dr.Read())
                //    {
                //        accounts newItem = new accounts();

                //        newItem.id = dr.GetInt32(0);
                //        newItem.name = dr.GetString(1);
                //        newItem.balance = dr.GetInt32(2);
                //        //newItem.TimeSpan = dr.GetString(3);
                //        //newItem.Price = dr.GetDecimal(4);
                //        //newItem.TypeName = dr.GetString(5);

                //        lstCustomer.Add(newItem);
                //    }
                //}


                
            }
            catch (Exception e)
            {
                Helper.SendSlackExceptionMsg($"Error from GetBilling Summary: hello", "GetBillingSummary");
                Helper.SendSlackEventMsg($"Error from GetBilling Summary: hello");
                Helper.LogException(e);

            }
            return lstCustomer;

        }

    }
}
