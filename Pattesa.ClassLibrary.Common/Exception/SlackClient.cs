﻿#region Copyright Pattesa © 2019
//
//  NAME:       SlackClient.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    Sending Project Error Messages in Slack 
//
#endregion

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Pattesa.ClassLibrary.Common.Exception
{
    public class SlackClient
    {
        private readonly Uri _uri;
        private readonly Encoding _encoding = new UTF8Encoding();
        public SlackClient(string urlWithAccessToken)
        {           
            _uri = new Uri(urlWithAccessToken);
        }

        /// <summary>
        /// Post a message using simple strings
        /// </summary>
        /// <param name="text"></param>
        /// <param name="username"></param>
        /// <param name="channel"></param>
        /// <param name="emoji"></param>
        /// <param name="icon"></param>
        public void PostMessage(string text, string username = null, string channel = null, string emoji = null, string icon = null)
        {
            Payload payload = new Payload()
            {
                channel = channel,
                username = username,
                text = text,
                emoji = emoji,
                icon = icon
            };
            PostMessage(payload);
        }

        /// <summary>
        /// Post a message using a Payload object
        /// </summary>
        /// <param name="payload"></param>
        public void PostMessage(Payload payload)
        {
            string payloadJson = JsonConvert.SerializeObject(payload);
            using (WebClient client = new WebClient())
            {
                NameValueCollection data = new NameValueCollection();
                data["payload"] = payloadJson;
                var response = client.UploadValues(_uri, "POST", data);
                //The response text is usually "ok"
                string responseText = _encoding.GetString(response);
                //Console.WriteLine(responseText);
            }
        }
    }

    /// <summary>
    /// Slack Model
    /// </summary>
    public class Payload
    {
        public string channel { get; set; }
        public string username { get; set; }
        public string text { get; set; }
        public string emoji { get; set; }
        public string icon { get; set; }
    }

}
