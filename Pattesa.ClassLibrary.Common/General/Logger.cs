﻿#region Copyright Pattesa © 2019
//
//  NAME:       Logger.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    Written Custom Log Files 
//
#endregion

using System;
using System.IO;
using System.Xml;
using log4net;
using System.Reflection;


namespace Pattesa.ClassLibrary.Common.General
{
    public class Logger
    {
        private static readonly string LOG_CONFIG_FILE = @"log4net.config";
        private static readonly log4net.ILog _log = GetLogger(typeof(Logger));
        private readonly Uri _uri;
        private static object args;

        public Logger(string urlWithAccessToken)
        {            
            _uri = new Uri(urlWithAccessToken);
        }

        public static ILog GetLogger(Type type)
        {
            return LogManager.GetLogger(type);
        }
        public void Debug(object message)
        {
            SetLog4NetConfiguration();
            _log.Debug(message);
        }

       /// <summary>
       /// Log Write Method
       /// </summary>
        private static void SetLog4NetConfiguration()
        {
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead(LOG_CONFIG_FILE));

            var repo = LogManager.CreateRepository(
                Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
            var logger = LogManager.GetLogger(typeof(log4net.Repository.Hierarchy.Hierarchy));
            logger.Error("hello");
           
        }

        


    }
}
