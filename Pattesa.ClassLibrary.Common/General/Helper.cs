﻿#region Copyright Pattesa © 2019
//
//  NAME:       Helper.cs
//  AUTHOR:     Baburao
//  COMPANY:    Pattesa
//  DATE:       11-30-2019
//  PURPOSE:    All Common Method And Variable Declare in All Projects 
//
#endregion

using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using Pattesa.ClassLibrary.Common.Exception;
using System.Xml;
using log4net;

namespace Pattesa.ClassLibrary.Common.General
{
    /// <summary>
    ///     Helper class
    /// </summary>
    public class Helper
    {
        #region Constant Char's
        /// <summary>
        ///     Obfusication character
        /// </summary>
        public const char ObfusicationCharacter = '*';

        private static string _envVersion = $"{"dev"}{"1.0"}{":"}";

        private static string LOG_CONFIG_FILE = "C:\\angularprojects\\log.txt";

        #endregion


        #region "Slack"
        /// <summary>
        /// //Dev Events
        //Dev
        //pattesa-dev-exceptions
        //https://hooks.slack.com/services/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
        //pattesa-dev-events
        //https://hooks.slack.com/services/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
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="subject"></param>
        /// <param name="sendMail"></param>
        ///
        /// 
        public static async void SendSlackExceptionMsg(string msg, string subject = "", bool sendMail = false)
        {
            try
            {
                
                
                msg = _envVersion + msg;
                Logger log1 = new Logger(msg);
                log1.Debug(msg);
                string urlWithAccessToken = "https://hooks.slack.com/services/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";
                string channel = "dev-exceptions";
                SlackClient client = new SlackClient(urlWithAccessToken);
                client.PostMessage(username: "", text: $"Error: {msg}", channel: channel, icon: null);
            }
            catch
            {
                
            }

        }

        /// <summary>
        /// Get Calling For Only Exceptios.
        /// </summary>
        /// <param name="methodBase"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private static string GetCallForExceptionThisMethod(MethodBase methodBase, System.Exception e)
        {
            StackTrace trace = new StackTrace(e);
            StackFrame previousFrame = null;
            foreach (StackFrame frame in trace.GetFrames())
            {
                if (frame.GetMethod() == methodBase)
                {
                    break;
                }

                previousFrame = frame;
            }

            return previousFrame != null ? previousFrame.GetMethod().Name : null;
        }

        /// <summary>
        /// Log Exceptions for Slack and Custom Log Files
        /// </summary>
        /// <param name="ex"></param>
        public static async void LogException(System.Exception ex)
        {
            string msg = string.Empty;
            string subject = string.Empty;
            bool sendMail = false;
            StringBuilder errorMessages = new StringBuilder();
            if (ex is SqlException)
            {
                sendMail = true;
                subject = "Api Exception! " + GetCallForExceptionThisMethod(MethodBase.GetCurrentMethod(), ex);
                for (int i = 0; i < (ex as SqlException).Errors.Count; i++)
                {
                    errorMessages.Append("Index #" + i + "\n" +
                                         "Message: " + (ex as SqlException).Errors[i].Message + "\n<br/>" +
                                         "LineNumber: " + (ex as SqlException).Errors[i].LineNumber + "\n<br/>" +
                                         "Source: " + (ex as SqlException).Errors[i].Source + "\n<br/>" +
                                         "Procedure: " + (ex as SqlException).Errors[i].Procedure + "\n<br/>"
                                         );
                }
                errorMessages.Append("Stack Trace: " + (ex as SqlException).StackTrace.ToString());
                msg = errorMessages.ToString();
            }
            else
            {
                sendMail = false;
                subject = "Api Exception! " + GetCallForExceptionThisMethod(MethodBase.GetCurrentMethod(), ex);
                System.Exception realError = ex;
                while (realError.InnerException != null)
                    realError = realError.InnerException;
                msg = $"Source: {realError.Source} Message{realError.ToString()}";// Stack Trace: {realError.StackTrace.ToString()}";
                Logger log1 = new Logger(msg);
                log1.Debug(msg);
            }
            try
            {
                msg = _envVersion + msg;             
                string urlWithAccessToken = "https://hooks.slack.com/services/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";
                string channel = "dev-exceptions";
                SlackClient client = new SlackClient(urlWithAccessToken);
                client.PostMessage(username: "", text: $"Error: {msg}", channel: channel, icon: null);                
            }
            catch (System.Exception e)
            {
            }
        }

        /// <summary>
        /// Send Slack Event Messages
        /// </summary>
        /// <param name="msg"></param>
        public static void SendSlackEventMsg(string msg)
        {
            msg = _envVersion + msg;
            try
            {
                string urlWithAccessToken = "https://hooks.slack.com/services/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";
                string channel = "dev-events";
                SlackClient client = new SlackClient(urlWithAccessToken);
                client.PostMessage(username: "", text: $"Event: {msg}", channel: channel, icon: null);
            }
            catch (System.Exception e)
            {
                //Console.WriteLine(e.Message);
                // Ignore slack errors 
            }
        }

        #endregion
    }
}
